import unittest
import random

from unittest.mock import patch
from mysound import Sound
from soundops import soundadd


class TestSoundAdd(unittest.TestCase):

    def rand_list(self, duracion):
        n = 10 * duracion

        min_value = 0
        max_value = 100

        list = []

        for i in range(n):
            rand_int = random.randint(min_value, max_value)
            list.append(rand_int)
        return (list)

    @patch.object(Sound, 'samples_second', 0)
    def test_same(self):
        duracion1 = 1
        duracion2 = 1
        s1 = Sound(duracion1)
        s2 = Sound(duracion2)
        s1.buffer = self.rand_list(duracion1)
        s2.buffer = self.rand_list(duracion2)
        result = soundadd(s1, s2)
        self.assertEqual(len(result.buffer), len(s1.buffer))
        self.assertEqual(len(result.buffer), len(s2.buffer))
        self.assertEqual(result.duration, duracion1)

    @patch.object(Sound, 'samples_second', 0)
    def test_longers1(self):
        duracion1 = 2
        duracion2 = 1
        s1 = Sound(duracion1)
        s2 = Sound(duracion2)
        s1.buffer = self.rand_list(duracion1)
        s2.buffer = self.rand_list(duracion2)
        result = soundadd(s1, s2)
        self.assertEqual(len(result.buffer), len(s1.buffer))
        self.assertNotEquals(len(result.buffer), len(s2.buffer))
        self.assertEqual(result.duration, duracion1)

    @patch.object(Sound, 'samples_second', 0)
    def test_longers2(self):
        duracion1 = 1
        duracion2 = 2
        s1 = Sound(duracion1)
        s2 = Sound(duracion2)
        s1.buffer = self.rand_list(duracion1)
        s2.buffer = self.rand_list(duracion2)
        result = soundadd(s1, s2)
        self.assertEqual(len(result.buffer), len(s2.buffer))
        self.assertNotEquals(len(result.buffer), len(s1.buffer))
        self.assertEqual(result.duration, duracion2)


if __name__ == '__main__':
    unittest.main()
