#Ejercicio 6
import unittest
from mysoundsin import SoundSin


class TestSoundSin(unittest.TestCase):

    def test_validamp(self):

        if isinstance(SoundSin.max_amplitude, str):
            raise TypeError("max_amplitude must be numeric")

        return SoundSin.max_amplitude

    def test_validsam(self):

        if isinstance(SoundSin.samples_second, str):
            raise TypeError("samples_second must be integer")

        elif isinstance(SoundSin.samples_second, float):
            raise TypeError("samples_second must be integer")

        elif SoundSin.samples_second < 0:
            raise ValueError("samples_second must be positive")

        return SoundSin.samples_second

    def test_nsmaples(self):
        soundsin = SoundSin(2,440, 10000)
        self.assertTrue(44100*soundsin.duration, len(soundsin.buffer))
        self.assertTrue(44100*soundsin.duration, soundsin.nsamples)

    def test_period(self):
        soundsin = SoundSin(2, 440, 10000)
        period = 44100 / 440
        self.assertAlmostEqual(10000, soundsin.buffer[int(period * 0.25)],delta=2)
        self.assertAlmostEqual(10000, soundsin.buffer[int(period * 1.25)],delta=2)








