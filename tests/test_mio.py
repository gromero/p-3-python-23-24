# Ejercicio 4
import unittest
from mysound import Sound


class TestInvalid(unittest.TestCase):
    def test_validamp(self):

        if isinstance(Sound.max_amplitude, str):
            raise TypeError("max_amplitude must be numeric")

        return Sound.max_amplitude

    def test_validsam(self):

        if isinstance(Sound.samples_second, str):
            raise TypeError("samples_second must be integer")

        elif isinstance(Sound.samples_second, float):
            raise TypeError("samples_second must be integer")

        elif Sound.samples_second < 0:
            raise ValueError("samples_second must be positive")

        return Sound.samples_second
