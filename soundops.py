# Ejercicio 7
from mysound import Sound


def soundadd(s1: Sound, s2: Sound) -> Sound:
    result = Sound(max(s1.duration, s2.duration))
    max_samples = max(len(s1.buffer), len(s2.buffer))
    for i in range(max_samples):
        if i < len(s1.buffer) and i < len(s2.buffer):
            result.buffer.append(s1.buffer[i] + s2.buffer[i])
        elif i < len(s1.buffer):
            result.buffer.append(s1.buffer[i])
        else:
            result.buffer.append(s2.buffer[i])
    return result
