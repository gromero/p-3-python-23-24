# Ejercicio 5
from mysound import Sound


class SoundSin(Sound):

    def __init__(self, duration, frecuency, amplitude):
        super().__init__(duration)
        super().sin(frecuency, amplitude)
